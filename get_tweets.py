#!/usr/bin/env python
# coding: utf-8

from tweepy import OAuthHandler, API
from tweepy import Stream
from tweepy.streaming import StreamListener
import json
from datetime import datetime
import sys
import os


consumer_key = 'qUqABnLA0lPyoLNFxV9OF5VFC'
consumer_secret = 'OZJmXthjf5APTlvmdHUyGBvW6IBj3BqQGax1ZAwLqZFxzbJsba'
access_token = '15257539-duK7OfwVvTih6r9bXsJQsB8fmEpMN7fFDzbIMhuGu'
access_secret = '3dv7Nxnb5aqm6yOSNM9f9UQ6CePb6TJDwtIECf2zpdBKP'

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
api = API(auth)

# In[8]:
counter = 0
tweets_per_hour = 6000

tweet_file_name = 'tweets/data.txt'


def get_column_names():
    columns = "{:>20}\t{:>15}\t{:>20}\t{:>50}\t".format("tw_id", "tw_created_at", "user_id", "user_screen_name")
    columns += "{:>50}\t{:>100}\t{:>100}\t{:>8}\t".format("name", "user_description", "user_location",
                                                          "user_followers_count")
    columns += "{:>8}\t{:>8}\t{:>8}\t{:>8}\t".format("user_friends_count", "user_listed_count", "user_statuses_count",
                                                     "user_favourites_count")
    columns += "{:>20}\t{:>20}\t{:>20}\t{:>10}\t".format("user_created_at", "tw_favorite_count", "tw_retweet_count",
                                                         "tw_is_quote")
    columns += "{:>15}\t{:>50}\t{:>12}\t{:>10}\t".format("tw_media_type", "tw_hashtags", "tw_has_url", "tw_is_quote")
    columns += "{:>20}\t{:>280}".format("is_original_tweet", "tw_text")
    # retweet info comes after the tweet data
    return columns + "\t" + columns + "\r\n"


def insert_column_names():
    out = open(tweet_file_name, "w", encoding="utf-8")
    out.write(get_column_names())
    out.close()


class TweetListener(StreamListener):
    def normalize_text(self, str):
        str = str.replace('\n', ' ').replace('\t', ' ').replace("\r", "")
        str = str.replace("ي", "ی")
        return str



    def __init__(self):
        self.counter = counter


    def generate_final_text(self, json_data, first_part=True):
        try:
            tweet_text = ""
            if 'extended_tweet' in json_data:
                if 'full_text' in json_data['extended_tweet']:
                    tweet_text = json_data['extended_tweet']['full_text']
                else:
                    tweet_text = json_data['text']
            else:
                tweet_text = json_data['text']
            # Tweet ID
            final_text = "{:>20}".format(str(json_data["id"]))

            # print("Tweet Date :" + json_data["created_at"])
            date_object = datetime.strptime(json_data["created_at"], '%a %b %d %H:%M:%S +0000 %Y')
            final_text += "\t{0:15}".format(date_object.strftime("%Y-%m-%d"))
            # print("User ID : " + str(json_data["user"]["id"]))
            final_text += "\t{0:>20}".format(str(json_data["user"]["id"]))
            # print("User Name : " + json_data["user"]["screen_name"])
            final_text += "\t{0:>50}".format(json_data["user"]["screen_name"])

            # print("User Name : " + json_data["user"]["name"])
            final_text += "\t{0:>50}".format(json_data["user"]["name"])

            # print("User Name : " + json_data["user"]["description"])
            final_text += "\t{0:>100}".format(self.normalize_text(str(json_data["user"]["description"])))

            # print("User Location :" + json.dumps(json_data["user"]["location"]))
            final_text += "\t{0:>100}".format(self.normalize_text(str(json_data["user"]["location"])))

            # print("User followers count :" + json.dumps(json_data["user"]["followers_count"]))
            final_text += "\t{0:>20}".format(str(json_data["user"]["followers_count"]))

            # print("User friends count :" + json.dumps(json_data["user"]["friends_count"]))
            final_text += "\t{0:>20}".format(str(json_data["user"]["friends_count"]))

            # print("User friends count :" + json.dumps(json_data["user"]["listed_count"]))
            final_text += "\t{0:>20}".format(str(json_data["user"]["listed_count"]))

            # print("User friends count :" + json.dumps(json_data["user"]["statuses_count"]))
            final_text += "\t{0:>20}".format(str(json_data["user"]["statuses_count"]))

            # print("User friends count :" + json.dumps(json_data["user"]["favourites_count"]))
            final_text += "\t{0:>20}".format(str(json_data["user"]["favourites_count"]))

            # print("User friends count :" + json.dumps(json_data["user"]["created_at"]))
            user_date_object = datetime.strptime(json_data["user"]["created_at"], '%a %b %d %H:%M:%S +0000 %Y')
            final_text += "\t{0:20}".format(user_date_object.strftime("%Y-%m-%d"))


            # print("Tweet Reply count :" + json.dumps(json_data["favorite_count"]))
            final_text += "\t{0:>20}".format(str(json_data["favorite_count"]))
            # print("Tweet retweet count :" + json.dumps(json_data["retweet_count"]))
            final_text += "\t{0:>20}".format(str(json_data["retweet_count"]))

            # print("Tweet retweet count :" + json.dumps(json_data["is_quote_status"]))
            if json_data["is_quote_status"]:
                is_quote_status = 1
            else:
                is_quote_status = 0

            final_text += "\t{0:>10}".format(str(is_quote_status))

            # Media Type
            if 'extended_tweet' in json_data:
                if "media" in json_data["extended_tweet"]["entities"]:
                    if len(json_data["extended_tweet"]["entities"]["media"]) == 0:
                        final_text += "\t{0:>15}".format(str("None"))
                    else:
                        final_text += "\t{0:>15}".format(json_data["extended_tweet"]["entities"]["media"][0]["type"])
                else:
                    final_text += "\t{0:>15}".format(str("None"))
            elif "extended_entities" in json_data:
                if "media" in json_data["extended_entities"]:
                    if len(json_data["extended_entities"]["media"]) == 0:
                        final_text += "\t{0:>15}".format(str("None"))
                    else:
                        final_text += "\t{0:>15}".format(json_data["extended_entities"]["media"][0]["type"])
                else:
                    final_text += "\t{0:>15}".format(str("None"))
            else:
                if "media" in json_data["entities"]:
                    if len(json_data["entities"]["media"]) == 0:
                        final_text += "\t{0:>15}".format(str("None"))
                    else:
                        final_text += "\t{0:>15}".format(json_data["entities"]["media"][0]["type"])
                else:
                    final_text += "\t{0:>15}".format(str("None"))

            hashtags_data = json_data["entities"]["hashtags"]
            if len(hashtags_data) == 0:
                final_text += "\t{0:>50}".format(str("None"))
            else:
                hashtags = ""
                for hashtag in hashtags_data:
                    hashtags += hashtag["text"] + "^^"
                if hashtags.endswith("^^"):
                    hashtags = hashtags[:-2]
                final_text += "\t{0:>50}".format(hashtags)

            has_url = 0
            urls = json_data["entities"]["urls"]
            if len(urls) > 0:
                has_url = 1
            final_text += "\t{0:>12}".format(str(has_url))
            if first_part:
                if tweet_text.startswith("RT @"):
                    # Not Original Tweet
                    final_text += "\t{0:>20}".format(str(0))

                    # full_text = api.get_status(json_data["retweeted_status"]["id"], tweet_mode='extended')._json['full_text']
                    # tweet_text = tweet_text[:tweet_text.find(":") + 1] + full_text
                    # # print(json_data["text"])
                    # # print(tweet_text)
                    # open("tweets/RT.json", "w").write(json.dumps(api.get_status(json_data["retweeted_status"]["id"], tweet_mode='extended')._json))
                    # # return False
                else:
                    # Original Tweet
                    final_text += "\t{0:>20}".format(str(1))
            else:
                final_text += "\t{0:>20}".format(str(1))

            # print("Tweet Text :" + json_data["text"].replace('\n', ' ').repalce('\t', ' '))
            final_text += "\t{:>280}".format(self.normalize_text(tweet_text))
            return final_text
        except BaseException as e:
            # print("Error on_data: %s" % str(e))
            # exc_type, exc_obj, exc_tb = sys.exc_info()
            # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            # open("error.json", "w").write(json.dumps(json_data))
            return e

    def on_data(self, data):
        json_data = json.loads(data)
        try:

            # tweet_file_name = 'tweets/'+datetime.now().strftime("%Y-%m-%d")+".txt"

            final_text = self.generate_final_text(json_data)

            if "RT" in json_data["text"]:
                original_text = self.generate_final_text(json_data["retweeted_status"], first_part=False)
                final_text = "{}\t{}\r\n".format(final_text, original_text)
            else:
                final_text += "\r\n"
            with open(tweet_file_name, 'a', encoding="utf-8") as f:
                self.counter += 1
                f.write(final_text)
                print(str(self.counter) + "\t" + json_data["text"])
            if self.counter > tweets_per_hour:
                #  terminate the program
                return False
            return True

        except BaseException as e:

            # exc_type, exc_obj, exc_tb = sys.exc_info()
            # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            # open("error.json", "w").write(json.dumps(json_data))
            return True

    def on_error(self, status):
        print(status)
        return True


insert_column_names()
try:
    twitter_stream = Stream(auth, TweetListener())
    twitter_stream.filter(languages=['fa'], track=['مشهد','طرقبه','شانذیز'])

except BaseException as e:

    # print("Error on_data: %s" % str(e))
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    twitter_stream = Stream(auth, TweetListener())
    twitter_stream.filter(languages=['fa'], track=['مشهد','طرقبه','شانذیز'])

